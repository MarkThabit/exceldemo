//
//  ViewController.swift
//  ExcelDemo
//
//  Created by Mark Maged on 12/23/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit
import xlsxwriter

class ViewController: UIViewController
{
    var docUrl: URL? {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
    }
    
    // MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        createExcelFile()
    }
    
    // MARK: - Helper Methods
    
    func createExcelFile()
    {
        guard let url = docUrl?.appendingPathComponent("demo.xlsx") else {
            return
        }
        
        // Excel File
        let workbook = workbook_new(url.path)
        
        // Excel Sheet
        let worksheet = workbook_add_worksheet(workbook, nil)
        
        // Create Format
        let format = workbook_add_format(workbook)
        
        /* Set the bold property for the format */
        format_set_bold(format)
        
        // Set width for the first coloumn to 20 with no format
        worksheet_set_column(worksheet, 0, 0, 20, nil)
        
        // Write string `Hello` to first cell with index 0, 0 with no format
        worksheet_write_string(worksheet, 0, 0, "Hello", nil)
        
        // Write string `Hello` to second cell of the first coloumn with index 1, 0 with format
        worksheet_write_string(worksheet, 1, 0, "World", format)
        
        /* Write some numbers. */
        worksheet_write_number(worksheet, 2, 0, 123,     nil)
        worksheet_write_number(worksheet, 3, 0, 123.456, nil)
        
        /* Insert an image. */
//        worksheet_insert_image(worksheet, 1, 2, "logo.png")
        
        workbook_close(workbook)
    }
}
